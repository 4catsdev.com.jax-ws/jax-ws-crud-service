# jax-ws-crud-service
Um exemplo de como criar um serviço CRUD (Create, Read, Update, Delete) baseado em SOAP. A aplicação oferece as operações de CRUD de uma entidade chamada Noticia (atributos: título, texto e data de publicação).

Vídeo no youtube: https://youtu.be/eaqTfvpvlvY

#### Características:
- Usa-se o maven para criar o projeto;
- É usado o jaxws-maven-plugin (wsgen) para gerar todos os artefatos portáteis para o serviço;
- É usado a versão 3.0.0 do jax-ws;
- Operações da aplicação: cadastrar, alterar, ler, remover e listar todos os objetos da entidade Noticia do banco de dados;
- Usa-se JDBC para operações em banco de dados PostgreSql.
